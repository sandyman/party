package com.droidroid.party;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class EventDB extends SQLiteOpenHelper {
	public static final String TABLE_EVENT = "events";
	public static final String COLUMN_EVENT_ID = "_id";
	public static final String COLUMN_EVENT_NAME = "ev_name";
	public static final String COLUMN_EVENT_DATE = "ev_date";
	public static final String[] PROJECTION = { COLUMN_EVENT_ID, COLUMN_EVENT_NAME, COLUMN_EVENT_DATE };
	private static final String DB_NAME = "events.db";
	private static final String[] CREATE_DATABASE_STATEMENTS = { "CREATE TABLE events (" + COLUMN_EVENT_ID
			+ " INTEGER PRIMARY KEY AUTOINCREMENT, " + COLUMN_EVENT_NAME + " varchar(100) NOT NULL, "
			+ COLUMN_EVENT_DATE + " INTEGER NOT NULL);" };

	public EventDB(Context context) {
		super(context, DB_NAME, null, CREATE_DATABASE_STATEMENTS.length);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		for (String sql : CREATE_DATABASE_STATEMENTS)
			db.execSQL(sql);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		for (int i = oldVersion; i < newVersion; i++)
			db.execSQL(CREATE_DATABASE_STATEMENTS[i]);
	}

}
