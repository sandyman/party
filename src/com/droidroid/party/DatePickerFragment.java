package com.droidroid.party;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.widget.DatePicker;

public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

	public interface OnDateSelectedListener {
		public void done();
	}

	private int mYear = 2013;
	private int mMonth = 11;
	private int mDay = 24;

	private OnDateSelectedListener mListener = null;

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		// Use the current date as the default date in the picker
		final Calendar c = Calendar.getInstance();
		int year = c.get(Calendar.YEAR);
		int month = c.get(Calendar.MONTH);
		int day = c.get(Calendar.DAY_OF_MONTH);

		// Create a new instance of DatePickerDialog and return it
		DatePickerDialog dpDialog = new DatePickerDialog(getActivity(), this, year, month, day);
		dpDialog.getDatePicker().setCalendarViewShown(false);
		dpDialog.getDatePicker().setMaxDate(Calendar.getInstance().getTimeInMillis());
		return dpDialog;
	}

	/**
	 * Set OnDateSelectedListener
	 * @param l
	 */
	public void setOnDateSelectedListener(OnDateSelectedListener l) {
		mListener = l;
	}

	/**
	 * Date set by user
	 */
	public void onDateSet(DatePicker view, int year, int month, int day) {
		mYear = year;
		mMonth = month;
		mDay = day;

		mListener.done();
	}

	/**
	 * Get the date
	 * @return
	 */
	public Date getDate() {
		return new GregorianCalendar(mYear, mMonth, mDay).getTime();
	}
}
