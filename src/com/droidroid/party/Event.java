package com.droidroid.party;

import java.util.Collection;
import java.util.Date;

public interface Event {
	Collection<Celebration> getAll(Date startDate, Date endDate);
}
