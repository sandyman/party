package com.droidroid.party;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

public class EventProvider extends ContentProvider {
	private static final String AUTHORITY = "com.droidroid.party";
	private static final String BASE_PATH = "events";
	public static final Uri LIST_URI = Uri.parse("content://" + AUTHORITY + "/" + BASE_PATH);
	private static final UriMatcher sURIMatcher = new UriMatcher(UriMatcher.NO_MATCH);
	private static final int EVENTS = 10;
	private static final int EVENT_ID = 20;
	static {
		sURIMatcher.addURI(AUTHORITY, BASE_PATH, EVENTS);
		sURIMatcher.addURI(AUTHORITY, BASE_PATH + "/#", EVENT_ID);
	}
	private EventDB database;

	@Override
	public boolean onCreate() {
		database = new EventDB(getContext());
		return false;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
		SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
		queryBuilder.setTables(EventDB.TABLE_EVENT);
		int uriType = sURIMatcher.match(uri);
		switch (uriType) {
		case EVENTS:
			break;
		case EVENT_ID:
			// adding the ID to the original query
			queryBuilder.appendWhere(EventDB.COLUMN_EVENT_ID + "=" + uri.getLastPathSegment());
			break;
		default:
			throw new IllegalArgumentException("Unknown URI: " + uri);
		}
		SQLiteDatabase db = database.getReadableDatabase();
		Cursor cursor = queryBuilder.query(db, projection, selection, selectionArgs, null, null,
				sortOrder == null ? EventDB.COLUMN_EVENT_DATE : sortOrder);
		// make sure that potential listeners are getting notified
		cursor.setNotificationUri(getContext().getContentResolver(), uri);
		return cursor;
	}

	@Override
	public String getType(Uri uri) {
		return null;
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		int uriType = sURIMatcher.match(uri);
		SQLiteDatabase sqlDB = database.getWritableDatabase();
		long id = 0;
		switch (uriType) {
		case EVENTS:
			id = sqlDB.insert(EventDB.TABLE_EVENT, null, values);
			break;
		default:
			throw new IllegalArgumentException("Unknown URI: " + uri);
		}
		getContext().getContentResolver().notifyChange(uri, null);
		return Uri.parse(BASE_PATH + "/" + id);
	}

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		int uriType = sURIMatcher.match(uri);
		SQLiteDatabase sqlDB = database.getWritableDatabase();
		int rowsDeleted = 0;
		switch (uriType) {
		case EVENTS:
			rowsDeleted = sqlDB.delete(EventDB.TABLE_EVENT, selection, selectionArgs);
			break;
		case EVENT_ID:
			String id = uri.getLastPathSegment();
			if (isEmpty(selection)) {
				rowsDeleted = sqlDB.delete(EventDB.TABLE_EVENT, EventDB.COLUMN_EVENT_ID + "=" + id, null);
			} else {
				rowsDeleted = sqlDB.delete(EventDB.TABLE_EVENT, EventDB.COLUMN_EVENT_ID + "=" + id + " and "
						+ selection, selectionArgs);
			}
			break;
		default:
			throw new IllegalArgumentException("Unknown URI: " + uri);
		}
		getContext().getContentResolver().notifyChange(uri, null);
		return rowsDeleted;
	}

	protected boolean isEmpty(String selection) {
		return selection == null || selection.length() == 0;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
		int uriType = sURIMatcher.match(uri);
		SQLiteDatabase sqlDB = database.getWritableDatabase();
		int rowsUpdated = 0;
		switch (uriType) {
		case EVENTS:
			rowsUpdated = sqlDB.update(EventDB.TABLE_EVENT, values, selection, selectionArgs);
			break;
		case EVENT_ID:
			String id = uri.getLastPathSegment();
			if (isEmpty(selection)) {
				rowsUpdated = sqlDB.update(EventDB.TABLE_EVENT, values, EventDB.COLUMN_EVENT_ID + "=" + id, null);
			} else {
				rowsUpdated = sqlDB.update(EventDB.TABLE_EVENT, values, EventDB.COLUMN_EVENT_ID + "=" + id + " and "
						+ selection, selectionArgs);
			}
			break;
		default:
			throw new IllegalArgumentException("Unknown URI: " + uri);
		}
		getContext().getContentResolver().notifyChange(uri, null);
		return rowsUpdated;
	}

}
