package com.droidroid.party;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import android.app.Activity;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class CelebrationActivity extends Activity implements LoaderManager.LoaderCallbacks<Cursor> {
	private static final int EVENT_LOADER = 1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.celebrations);
		getLoaderManager().initLoader(EVENT_LOADER, null, this);
	}

	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle args) {
		if (id == EVENT_LOADER)
			return new CursorLoader(this, EventProvider.LIST_URI, EventDB.PROJECTION, null, null, null);
		throw new IllegalArgumentException("don't know " + id);
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
		ListView listView = (ListView) findViewById(R.id.celebration_list);
		PartyEveryDay app = new PartyEveryDay();
		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("CET"));
		cal.clear();
		int dateIndex = data.getColumnIndex(EventDB.COLUMN_EVENT_DATE);
		int nameIndex = data.getColumnIndex(EventDB.COLUMN_EVENT_NAME);
		while (data.moveToNext()) {
			int dateCode = data.getInt(dateIndex);
			cal.set(dateCode / 10000, (dateCode / 100) % 100 - 1, dateCode % 100, 12, 0, 0);
			String eventName = data.getString(nameIndex);
			app.addEvent(new BasicEvent(eventName, cal.getTime()), 100);
		}
		cal.setTime(new Date());
		cal.add(Calendar.DATE, -14);
		Date startDate = cal.getTime();
		cal.add(Calendar.DATE, 28);
		Date endDate = cal.getTime();
		final ArrayList<Celebration> celebrations = new ArrayList<Celebration>(app.getAll(startDate, endDate));
		final LayoutInflater inflater = LayoutInflater.from(this);
		listView.setAdapter(new BaseAdapter() {
			private final DateFormat dateFormat = DateFormat.getDateTimeInstance();

			@Override
			public int getCount() {
				return celebrations.size();
			}

			@Override
			public Celebration getItem(int position) {
				return celebrations.get(position);
			}

			@Override
			public long getItemId(int position) {
				return position;
			}

			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
				if (convertView == null)
					convertView = inflater.inflate(R.layout.celebration_item, parent, false);
				Celebration item = getItem(position);
				TextView dateView = (TextView) convertView.findViewById(R.id.celebration_datetime);
				dateView.setText(dateFormat.format(item.date));
				TextView nameView = (TextView) convertView.findViewById(R.id.celebration_name);
				nameView.setText(item.excuse);
				return convertView;
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.celebrations, menu);
		return true;
	}

	public void events(MenuItem item) {
		Intent intent = new Intent(this, EventListActivity.class);
		startActivity(intent);
	}

	@Override
	public void onLoaderReset(Loader<Cursor> loader) {
		ListView listView = (ListView) findViewById(R.id.celebration_list);
		listView.setAdapter(null);
	}
}
