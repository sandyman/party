package com.droidroid.party;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.TimeZone;
import java.util.TreeSet;

public class PartyEveryDay {
	public static void main(String[] args) {
		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("CET"));
		cal.clear();

		PartyEveryDay app = new PartyEveryDay();
		cal.set(1977, Calendar.OCTOBER, 1, 12, 0, 0);
		app.addEvent(new Birthday("Jaap", cal.getTime()), 100);
		cal.set(1972, Calendar.MARCH, 26, 12, 0, 0);
		app.addEvent(new Birthday("Sander", cal.getTime()), 100);
		cal.set(2008, Calendar.SEPTEMBER, 23, 12, 0, 0);
		app.addEvent(new BasicEvent("Android's initial release", cal.getTime()), 50);
		cal.set(2011, Calendar.OCTOBER, 21, 12, 0, 0);
		app.addEvent(new BasicEvent("DutchAUG was formed", cal.getTime()), 70);
		cal.set(2012, Calendar.MAY, 16, 12, 0, 0);
		app.addEvent(new BasicEvent("Sander got married", cal.getTime()), 50);
		cal.set(2011, Calendar.SEPTEMBER, 17, 12, 0, 0);
		app.addEvent(new Birthday("Kyan", cal.getTime()), 30);
		for (int i = -7; i < 7; i++) {
			cal.set(2013, Calendar.NOVEMBER, 24 + i, 0, 0, 0);
			Date startDate = cal.getTime();
			cal.add(Calendar.DATE, 1);
			Date endDate = cal.getTime();
			for (Celebration partyTime : app.getAll(startDate, endDate)) {
				System.out.println(partyTime);
			}
		}
	}

	private final HashMap<Event, Integer> events = new HashMap<Event, Integer>();

	public void addEvent(BasicEvent event, int priority) {
		events.put(event, priority);
	}

	protected TreeSet<Celebration> getAll(Date startDate, Date endDate) {
		TreeSet<Celebration> partyTimes = new TreeSet<Celebration>();
		for (Entry<Event, Integer> entry : events.entrySet()) {
			Event event = entry.getKey();
			for (Celebration partyTime : event.getAll(startDate, endDate)) {
				partyTimes.add(partyTime);
			}
		}
		return partyTimes;
	}

	protected Celebration getBest(Date startDate, Date endDate) {
		int bestScore = 0;
		Celebration bestExcuse = null;
		for (Entry<Event, Integer> entry : events.entrySet()) {
			Event event = entry.getKey();
			Integer priority = entry.getValue();
			for (Celebration partyTime : event.getAll(startDate, endDate)) {
				if (priority - partyTime.complexity > bestScore) {
					bestExcuse = partyTime;
					bestScore = priority - partyTime.complexity;
				}
			}
		}
		return bestExcuse;
	}
}
