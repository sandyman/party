package com.droidroid.party;

import java.util.Date;

class Celebration implements Comparable<Celebration> {
	final Date date;
	final String excuse;
	final int complexity;

	public Celebration(Date date, String excuse, int complexity) {
		this.date = date;
		this.excuse = excuse;
		this.complexity = complexity;
	}

	@Override
	public String toString() {
		return this.date + " " + this.excuse;
	}

	@Override
	public int compareTo(Celebration o) {
		int result = date.compareTo(o.date);
		if (result != 0)
			return result;
		if (complexity < o.complexity)
			return -1;
		if (complexity > o.complexity)
			return +1;
		result = excuse.compareTo(o.excuse);
		return result;
	}
}