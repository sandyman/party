package com.droidroid.party;

import java.util.Calendar;
import java.util.Date;
import java.util.EnumSet;

public class Birthday extends BasicEvent {
	private final String name;

	public Birthday(String name, Date date) {
		super(name + " was born", date);
		this.name = name;
	}

	@Override
	protected void check() {
		super.check();
		for (int x = 1; x < 100; x++) {
			checkUntil(x, EnumSet.of(TimeUnit.YEAR), EnumSet.of(TimeUnit.MONTH, TimeUnit.WEEK, TimeUnit.DAY), 80);
			checkUntil(x, EnumSet.of(TimeUnit.YEAR, TimeUnit.MONTH), EnumSet.of(TimeUnit.DAY), 80);
			checkUntil(x, EnumSet.of(TimeUnit.YEAR), EnumSet.of(TimeUnit.MONTH, TimeUnit.DAY), 80);
			checkUntil(x, EnumSet.of(TimeUnit.YEAR), EnumSet.of(TimeUnit.MONTH), 60);
			checkUntil(x, EnumSet.of(TimeUnit.YEAR), EnumSet.of(TimeUnit.WEEK), 60);
			checkUntil(x, EnumSet.of(TimeUnit.YEAR), EnumSet.of(TimeUnit.DAY), 60);
			checkUntil(x, EnumSet.of(TimeUnit.MONTH, TimeUnit.WEEK), EnumSet.of(TimeUnit.DAY), 80);
			checkUntil(x, EnumSet.of(TimeUnit.MONTH), EnumSet.of(TimeUnit.WEEK, TimeUnit.DAY), 80);
			checkUntil(x, EnumSet.of(TimeUnit.MONTH), EnumSet.of(TimeUnit.WEEK), 60);
			checkUntil(x, EnumSet.of(TimeUnit.MONTH), EnumSet.of(TimeUnit.DAY), 60);
			checkUntil(x, EnumSet.of(TimeUnit.MONTH), EnumSet.of(TimeUnit.WEEK, TimeUnit.DAY), 80);
			checkUntil(x, EnumSet.of(TimeUnit.MONTH, TimeUnit.WEEK), EnumSet.of(TimeUnit.DAY), 80);
			checkUntil(x, EnumSet.of(TimeUnit.MONTH), EnumSet.of(TimeUnit.WEEK), 60);
			checkUntil(x, EnumSet.of(TimeUnit.MONTH), EnumSet.of(TimeUnit.DAY), 60);

			long amount = x;
			for (int y = 0; y < 11; y++) {
				checkUntil(amount, EnumSet.of(TimeUnit.WEEK), EnumSet.of(TimeUnit.DAY), 60);
				checkUntil(amount, EnumSet.of(TimeUnit.DAY), EnumSet.of(TimeUnit.HOUR), 60);
				checkUntil(amount, EnumSet.of(TimeUnit.HOUR), EnumSet.of(TimeUnit.MINUTE), 60);
				amount *= 10;
			}
		}
	}

	private void checkUntil(long amount, EnumSet<TimeUnit> added, EnumSet<TimeUnit> removed, int complexity) {
		Calendar cal = calendar();
		for (TimeUnit unit : added)
			unit.add(cal, amount);
		for (TimeUnit unit : removed)
			unit.add(cal, -amount);

		StringBuilder sb = new StringBuilder();
		sb.append("it's ");
		appendUnitList(sb, amount, removed);
		sb.append(" until ").append(name).append(" is ");
		appendUnitList(sb, amount, added);
		sb.append(" old");
		String excuse = sb.toString();
		Date partyDate = cal.getTime();
		Celebration celebration = new Celebration(partyDate, excuse, complexity);
		addCelebration(celebration);
	}
}
