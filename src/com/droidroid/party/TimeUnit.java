package com.droidroid.party;

import java.text.NumberFormat;
import java.util.Calendar;
import java.util.Locale;

enum TimeUnit {
	YEAR(Calendar.YEAR, 1, 1, 100), MONTH(Calendar.MONTH, 1, 10, YEAR.maxAmount * 12), WEEK(Calendar.WEEK_OF_YEAR, 1,
			10, YEAR.maxAmount * 52), DAY(Calendar.DATE, 10, 100, YEAR.maxAmount * 365), HOUR(Calendar.HOUR_OF_DAY,
			100, 100, DAY.maxAmount * 24), MINUTE(Calendar.MINUTE, 100, 1000, DAY.maxAmount * 60), SECOND(
			Calendar.SECOND, 1000, 1000, MINUTE.maxAmount * 60);
	private static final NumberFormat numberFormat = NumberFormat.getInstance(Locale.US);
	private static final int MAX_ADD_AMOUNT = 1000000000;
	private final int calField;
	public final long interestingStart1;
	public final long interestingStart2;
	public final long maxAmount;

	private TimeUnit(int calField, long interestingStart1, long interestingStart2, long maxAmount) {
		this.calField = calField;
		this.interestingStart1 = interestingStart1;
		this.interestingStart2 = interestingStart2;
		this.maxAmount = maxAmount;
	}

	public void add(Calendar cal, long total) {
		while (total >= MAX_ADD_AMOUNT) {
			cal.add(calField, 1000000000);
			total -= MAX_ADD_AMOUNT;
		}
		int remainder = (int) total;
		cal.add(calField, remainder);
	}

	public String friendlyName(long amount) {
		StringBuilder sb = new StringBuilder(20);
		sb.append(numberFormat.format(amount));
		sb.append(' ');
		sb.append(this.toString().toLowerCase());
		if (amount != 1)
			sb.append('s');
		return sb.toString();
	}
}