package com.droidroid.party;

import android.app.Activity;
import android.content.ContentValues;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;

public class AddEventActivity extends Activity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.simple_event);
	}

	public void submitEvent(View v) {
		EditText nameEdit = (EditText) findViewById(R.id.edit_name);
		DatePicker datePicker = (DatePicker) findViewById(R.id.edit_date);
		ContentValues values = new ContentValues();
		values.put(EventDB.COLUMN_EVENT_NAME, nameEdit.getText().toString());
		values.put(EventDB.COLUMN_EVENT_DATE, datePicker.getYear() * 10000 + (datePicker.getMonth() + 1) * 100
				+ datePicker.getDayOfMonth());
		getContentResolver().insert(EventProvider.LIST_URI, values);
		finish();
	}
}
