package com.droidroid.party;

import java.util.Date;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.SimpleCursorAdapter.ViewBinder;
import android.widget.TextView;

public class EventListActivity extends Activity implements OnLongClickListener, LoaderManager.LoaderCallbacks<Cursor> {
	private static final int EVENT_LOADER = 1;

	private final String PREFS = "partyPrefs";
	private final String PREF_DOB = "prefDateOfBirth";

	private TextView mDob;

	Date mDateOfBirth = null;

	private SimpleCursorAdapter adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.events);
		String[] fromColumns = { EventDB.COLUMN_EVENT_NAME, EventDB.COLUMN_EVENT_DATE };
		int[] toFields = { R.id.event_name, R.id.event_date };
		ListView listView = (ListView) findViewById(R.id.event_list);
		adapter = new SimpleCursorAdapter(this, R.layout.event_item, null, fromColumns, toFields, 0);
		adapter.setViewBinder(new ViewBinder() {
			@Override
			public boolean setViewValue(View view, Cursor cursor, int columnIndex) {
				if (columnIndex == 2) {
					int value = cursor.getInt(columnIndex);
					int yyyy = value / 10000;
					int m = (value / 100) % 100;
					int d = value % 100;
					TextView textView = (TextView) view;
					textView.setText(d + "-" + m + "-" + yyyy);
					return true;
				}
				return false;
			}
		});
		listView.setAdapter(adapter);
		registerForContextMenu(listView);
		getLoaderManager().initLoader(EVENT_LOADER, null, this);
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.events_context_menu, menu);
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		Log.e("Main", "item delete!");
		if (item.getItemId() == R.id.action_delete_event) {
			AdapterContextMenuInfo menuInfo = (AdapterContextMenuInfo) item.getMenuInfo();
			int position = menuInfo.position;
			long id = adapter.getItemId(position);
			Log.e("Main", "item delete! " + id);
			getContentResolver().delete(EventProvider.LIST_URI, "_id=" + id, null);
		}
		return super.onContextItemSelected(item);
	}

	public void addBirthday(MenuItem item) {
		Intent intent = new Intent(this, AddEventActivity.class);
		startActivity(intent);
	}

	public void addEvent(MenuItem item) {
		Intent intent = new Intent(this, AddEventActivity.class);
		startActivity(intent);
	}

	/**
	 * Show date dialog and store selected date
	 */
	private void showDateDialog() {
		final DatePickerFragment newFragment = new DatePickerFragment();
		newFragment.setOnDateSelectedListener(new DatePickerFragment.OnDateSelectedListener() {

			@Override
			public void done() {
				storeNewDate(newFragment.getDate());
				mDob.setText(Util.date2string(mDateOfBirth));
			}
		});
		((DialogFragment) newFragment).show(getFragmentManager(), "datePicker");
	}

	/**
	 * Store new date in shared preferences
	 * 
	 * @param date
	 */
	private void storeNewDate(final Date date) {
		mDateOfBirth = date;
		SharedPreferences.Editor editor = getSharedPreferences(PREFS, 0).edit();
		editor.putString(PREF_DOB, Util.date2string(mDateOfBirth));
		editor.commit();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onLongClick(View v) {
		showDateDialog();
		return false;
	}

	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle args) {
		if (id == EVENT_LOADER)
			return new CursorLoader(this, EventProvider.LIST_URI, EventDB.PROJECTION, null, null, null);
		throw new IllegalArgumentException("don't know " + id);
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
		adapter.changeCursor(data);
	}

	@Override
	public void onLoaderReset(Loader<Cursor> loader) {
		adapter.changeCursor(null);
	}
}
