package com.droidroid.party;

import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Util {
	/**
	 * Convert string to date
	 * @param date
	 * @return
	 */
	public static Date string2date(final String dateString) throws ParseException {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
		Date convertedDate = new Date();
		convertedDate = dateFormat.parse(dateString);
		return convertedDate;
	}

	/**
	 * Convert date to string
	 * @param date
	 * @return
	 */
	public static String date2string(final Date date) {
		Format formatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
		String s = formatter.format(date);
		return s;
	}
}
