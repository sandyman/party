package com.droidroid.party;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.EnumSet;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.TimeZone;
import java.util.TreeMap;
import java.util.TreeSet;

public class BasicEvent implements Event {
	private final TreeSet<Celebration> partyTimes;
	private final String event;
	private final Date date;
	private final TimeZone timeZone;

	public BasicEvent(String event, Date date) {
		this.event = event;
		this.date = date;
		this.timeZone = TimeZone.getTimeZone("CET");
		this.partyTimes = new TreeSet<Celebration>();
		// System.out.println("checked " + this);
	}

	@Override
	public Collection<Celebration> getAll(Date startDate, Date endDate) {
		TreeMap<Date, Celebration> result = new TreeMap<Date, Celebration>();
		for (Celebration partyTime : partyTimes()) {
			Date partyDate = partyTime.date;
			if (partyDate.after(startDate) && partyDate.before(endDate))
				result.put(chop(partyDate), partyTime);
		}
		return result.values();
	}

	private TreeSet<Celebration> partyTimes() {
		if (partyTimes.isEmpty())
			this.check();
		return partyTimes;
	}

	private Date chop(Date dateTime) {
		Calendar cal = Calendar.getInstance(timeZone);
		cal.setTime(dateTime);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return cal.getTime();
	}

	protected void check() {
		for (TimeUnit u : TimeUnit.values()) {
			checkSimple(u);
		}
		for (TimeUnit u : TimeUnit.values()) {
			for (TimeUnit smallestUsedUnit : EnumSet.range(u, TimeUnit.SECOND))
				if (smallestUsedUnit != u) {
					checkSet(u, smallestUsedUnit);
				}
		}
		for (int x = 1; x < 100; x++) {
			if (x >= 4) {
				checkWithValues(EnumSet.of(TimeUnit.YEAR, TimeUnit.MONTH, TimeUnit.WEEK, TimeUnit.DAY),
						Arrays.asList(x, x - 1, x - 2, x - 3), 40);
				checkWithValues(EnumSet.of(TimeUnit.MONTH, TimeUnit.WEEK, TimeUnit.DAY),
						Arrays.asList(10 * x, 10 * x - 1, 10 * x - 2, 10 * x - 3), 40);
				checkWithValues(EnumSet.of(TimeUnit.MONTH, TimeUnit.WEEK, TimeUnit.DAY),
						Arrays.asList(10 * x, 10 * x - 10, 10 * x - 20, 10 * x - 30), 40);
			}
			if (x >= 3) {
				checkWithValues(EnumSet.of(TimeUnit.YEAR, TimeUnit.MONTH, TimeUnit.WEEK),
						Arrays.asList(x, x - 1, x - 2), 40);
				checkWithValues(EnumSet.of(TimeUnit.YEAR, TimeUnit.MONTH, TimeUnit.DAY),
						Arrays.asList(x, x - 1, x - 2), 40);
				checkWithValues(EnumSet.of(TimeUnit.MONTH, TimeUnit.WEEK, TimeUnit.DAY),
						Arrays.asList(x, x - 1, x - 2), 40);
			}
		}
		for (int x = 1; x < 10; x++) {
			for (TimeUnit u1 : EnumSet.range(TimeUnit.YEAR, TimeUnit.DAY)) {
				for (TimeUnit u2 : EnumSet.range(u1, TimeUnit.DAY)) {
					if (u1 != u2) {
						checkWithValues(EnumSet.of(u1, u2), Arrays.asList(x, x), 25);
					}
				}
			}
		}
	}

	protected void checkSimple(TimeUnit unit) {
		checkSimple(1, unit, 0);
		for (int x = 2; x < 10; x++) {
			checkSimple(x * unit.interestingStart1, unit, 1);
		}
		for (int x = 11; x < 100; x++) {
			checkSimple(x * unit.interestingStart2, unit, 10);
		}
	}

	protected void checkSimple(long total, TimeUnit u, int complexity) {
		while (total < u.maxAmount) {
			Calendar cal = calendar();
			u.add(cal, total);
			Celebration partyTime = new Celebration(cal.getTime(), "it's " + u.friendlyName(total) + " since " + event,
					complexity);
			addCelebration(partyTime);
			total *= 10;
		}
	}

	protected void checkSet(TimeUnit bigUnit, TimeUnit smallUnit) {
		checkSet(1, bigUnit, smallUnit, 2);
		for (int x = 1; x < 10; x++) {
			checkSet(x * bigUnit.interestingStart1, bigUnit, smallUnit, 5);
		}
		for (int x = 11; x < 100; x++) {
			checkSet(x * bigUnit.interestingStart2, bigUnit, smallUnit, 20);
		}
	}

	protected void checkSet(int x, TimeUnit bigUnit, TimeUnit smallUnit, int complexity) {
		long amount = x;
		while (amount <= bigUnit.maxAmount) {
			checkSet(amount, bigUnit, smallUnit, complexity);
			amount *= 10;
		}
	}

	protected void checkSet(long amount, TimeUnit bigUnit, TimeUnit smallUnit, int complexity) {
		EnumSet<TimeUnit> checkset = EnumSet.range(bigUnit, smallUnit);
		checkWithValues(checkset, infinite(amount), complexity);
		if (checkset.contains(TimeUnit.WEEK) && bigUnit != TimeUnit.WEEK && smallUnit != TimeUnit.WEEK) {
			checkset.remove(TimeUnit.WEEK);
			checkWithValues(checkset, infinite(amount), complexity);
		}
	}

	<E> Iterable<E> infinite(final E item) {
		return new Iterable<E>() {
			@Override
			public Iterator<E> iterator() {
				return new Iterator<E>() {
					@Override
					public boolean hasNext() {
						return true;
					}

					@Override
					public E next() {
						return item;
					}

					@Override
					public void remove() {
						throw new UnsupportedOperationException();
					}
				};
			}
		};
	}

	protected void checkWithValues(EnumSet<TimeUnit> units, Iterable<? extends Number> amounts, int complexity) {
		Iterator<TimeUnit> iterUnit = units.iterator();
		Iterator<? extends Number> iterAmount = amounts.iterator();

		Calendar cal = calendar();

		StringBuilder sb = new StringBuilder();
		sb.append("it's ");
		TimeUnit unit = iterUnit.next();
		long amount = iterAmount.next().longValue();
		unit.add(cal, amount);
		sb.append(unit.friendlyName(amount));
		while (iterUnit.hasNext()) {
			unit = iterUnit.next();
			amount = iterAmount.next().longValue();
			unit.add(cal, amount);
			sb.append(iterUnit.hasNext() ? ", " : " and ");
			sb.append(unit.friendlyName(amount));
		}
		sb.append(" since " + event);
		String excuse = sb.toString();
		Date partyDate = cal.getTime();
		addCelebration(new Celebration(partyDate, excuse, complexity));
	}

	protected void addCelebration(Celebration celebration) {
		partyTimes.add(celebration);
	}

	protected GregorianCalendar calendar() {
		GregorianCalendar cal = new GregorianCalendar(timeZone);
		cal.setTime(date);
		return cal;
	}

	protected static void appendUnitList(StringBuilder sb, long amount, EnumSet<TimeUnit> units) {
		Iterator<TimeUnit> iterUnit = units.iterator();
		TimeUnit unit = iterUnit.next();
		sb.append(unit.friendlyName(amount));
		while (iterUnit.hasNext()) {
			unit = iterUnit.next();
			sb.append(iterUnit.hasNext() ? ", " : " and ");
			sb.append(unit.friendlyName(amount));
		}
	}
}
